import React, { Component } from "react";
import { MenuButton, Logo } from "../components/header/header";
import { HeaderBackButton } from "react-navigation";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

export default class SettingsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)} />,
      headerTitle: <Logo />,
      headerBackTitle: "Settings",
      headerLayoutPreset: "center"
    };
  };
  render() {
    return(
      <View style={styles.container}>
      
        <TouchableOpacity style={styles.buttonStyle} onPress={this._signOutAsync}>
            <Text style={styles.buttonTextStyle}>Log out</Text>
        </TouchableOpacity>
        
      </View>
    ); 
  }

  _signOutAsync = async () => {
    console.log(`Sign out Async`);
    //console.log('Props:',this.props);
    try {
      await AsyncStorage.removeItem('userToken')
      this.props.navigation.navigate('Auth');
    } catch(e) {
      console.log('Sign out Error:',e);
      alert('Logout error: ' + e);
    }
    
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  buttonStyle: {
    width: "60%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#428AF8",
    marginBottom: 12,
    marginTop: 30,
    paddingVertical: 12,
    borderRadius: 4,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "rgba(255,255,255,0.7)"
  },
  buttonTextStyle: {
    color: "#FFF",
    textAlign: "center",
    height: 20
  },
});