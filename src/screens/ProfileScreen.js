import React from "react";
import { MenuButton, Logo } from "../components/header/header";
import { dispatcherURL as domain } from '/Projects/tabTest/app.json';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button,
  ImageBackground,
  Dimensions,
  SectionList,
} from 'react-native';

import defaultUser from "../assets/images/default-user.png";

export default class ProfileScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: <MenuButton onPress={() => navigation.openDrawer()} />,
      headerTitle: <Logo />,
      headerBackTitle: "Profile",
      headerLayoutPreset: "center"
    };
  };

  constructor(props) {
    super(props)
  }

  componentWillUnmount() {
    console.log('$$$$$ HomeScreen componentWillUnmount $$$$$');
  }

  componentDidMount() {
    //console.log('*****Home screen did mount*****:',this.props);
    console.log('##### HomeScreen componentDidMount #####');
  }

  componentDidUpdate() {
    //console.log('$$$$$$$$ HOME SCREEN COMPONENT DID UPDATE');
    //console.log('PROPS:',this.props);
  }

  render() {

    let basicInfo, contactInfo, conditions, disability, foodAllergy, medicationAllergy = [];

    if (this.props.subscriber) {
      basicInfo = [
        { id: 1, title: 'Name: ' + this.props.subscriber.first_name + ' ' + this.props.subscriber.last_name },
        { id: 2, title: 'Sex: ' + this.props.subscriber.sex },
        { id: 3, title: 'Date of birth: ' + this.props.subscriber.dob },
        { id: 4, title: 'Age: ' + this.props.subscriber.age },
        { id: 5, title: 'Height(cm): ' + this.props.subscriber.height_cm },
        { id: 6, title: 'Weight(kg): ' + this.props.subscriber.weight_kg },
        { id: 7, title: 'Blood type: ' + this.props.subscriber.blood_type },
      ];

      contactInfo = [
        { id: 1, title: 'Email: ' + this.props.subscriber.email },
        { id: 2, title: 'Phone: ' + this.props.subscriber.phone_no },
        { id: 3, title: 'Address line 1: ' + this.props.subscriber.address_1 },
        { id: 4, title: 'Address line 2: ' + this.props.subscriber.address_2 },
        { id: 5, title: 'City: ' + this.props.subscriber.city },
      ];

      if (this.props.subscriber.medicalConditions) {

        if (typeof (this.props.subscriber.medicalConditions.Condition) != "undefined") {
          conditions = this.props.subscriber.medicalConditions.Condition.map(function (e, i) {
            return { id: i, title: e.Condition };
          });
        }
        else {
          conditions = [{ id: 1, title: 'No conditions' }];
        }

        if (typeof (this.props.subscriber.medicalConditions.Disability) != "undefined") {
          disability = this.props.subscriber.medicalConditions.Disability.map(function (e, i) {
            return { id: i, title: e.Condition };
          });
        }
        else {
          disability = [{ id: 1, title: 'No disabilities' }];
        }

        if (typeof (this.props.subscriber.medicalConditions['Food Allergy']) != "undefined") {
          foodAllergy = this.props.subscriber.medicalConditions['Food Allergy'].map(function (e, i) {
            return { id: i, title: e.Condition };
          });
        }
        else {
          foodAllergy = [{ id: 1, title: 'No food allergies' }];
        }

        if (typeof (this.props.subscriber.medicalConditions['Medication Allergy']) != "undefined") {
          medicationAllergy = this.props.subscriber.medicalConditions['Medication Allergy'].map(function (e, i) {
            return { id: i, title: e.Condition };
          });
        }
        else {
          medicationAllergy = [{ id: 1, title: 'No medication allergies' }];
        }

      }

    }

    let hasSubscriber = (
      <View style={styles.container}>

        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>

          <View style={styles.welcomeContainer}>
            <Image
              source={{ uri: domain + this.props.subscriber.profile_picture }}
              style={styles.welcomeImage} />
          </View>

          <SectionList
            sections={[
              { title: 'Basic Info', data: basicInfo },
              { title: 'Conditions', data: conditions },
              { title: 'Disabilities', data: disability },
              { title: 'Food Allergies', data: foodAllergy },
              { title: 'Medication Allergies', data: medicationAllergy },
              { title: 'Contact Info', data: contactInfo },
            ]}
            renderItem={({ item }) => (
              <View style={styles.row}>
                <Text>{item.title}</Text>
              </View>
            )}
            renderSectionHeader={({ section }) => (
              <View style={styles.sectionHeader}>
                <Text>{section.title}</Text>
              </View>
            )}
            keyExtractor={item => item.id}
          />

          {/* <View style={styles.getStartedContainer}>
            <Text>
              First Name: {JSON.stringify(this.props.subscriber.first_name)}
            </Text>
            <Text>
              Last Name: {JSON.stringify(this.props.subscriber.last_name)}
            </Text>
            <Text>
              Email: {JSON.stringify(this.props.subscriber.email)}
            </Text>
            <Text>
              Phone: {JSON.stringify(this.props.subscriber.phone_no)}
            </Text>            
            <Text>
              Sex: Email: {JSON.stringify(this.props.subscriber.sex)}
            </Text>
            <Text>
              Date of Birth: {JSON.stringify(this.props.subscriber.dob)}
            </Text>
            <Text>
              Address line 1: {JSON.stringify(this.props.subscriber.address_1)}
            </Text>
            <Text>
              Address line 2: {JSON.stringify(this.props.subscriber.address_2)}
            </Text>
            <Text>
              City: {JSON.stringify(this.props.subscriber.city)}
            </Text>
            <Text>
              Blood type: {JSON.stringify(this.props.subscriber.blood_type)}
            </Text>
            <Text>
              Height(cm): {JSON.stringify(this.props.subscriber.height_cm)}
            </Text>
            <Text>
              Weight(kg): {JSON.stringify(this.props.subscriber.weight_kg)}
            </Text>
            <Text>
              Age: {JSON.stringify(this.props.subscriber.age)}
            </Text>
            <Text>
              Medical Conditions: {JSON.stringify(this.props.subscriber.medicalConditions)}
            </Text>
          </View> */}

        </ScrollView>
      </View>
    );
    return this.props.subscriber ? hasSubscriber :
      <View style={styles.container}>
        <View style={styles.imgContainer}>
          <View style={styles.outerCircle}>
            <Image source={defaultUser} style={styles.logo} />
          </View>
        </View>
      </View>
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c5c5c5',
  },
  row: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  sectionHeader: {
    backgroundColor: 'lightskyblue',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  imgContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'lightgray',
  },
  outerCircle: {
    width: 200,
    height: 200,
    borderRadius: 200 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  logo: {
    flex: 1,
    width: 120,
    height: 120,
    resizeMode: 'contain',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});

