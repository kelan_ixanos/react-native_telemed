import {dispatcherURL as domain} from '/Projects/tabTest/app.json';
import { Alert } from 'react-native';
import imageLogo from "../assets/images/logoTL.png";
import React, { Component } from "react";
import AsyncStorage from '@react-native-community/async-storage';
import {
    ActivityIndicator,
    Platform,
    StatusBar,
    StyleSheet,
    View,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
  } from 'react-native';

export default class SignInScreen extends React.Component {
    static navigationOptions = {
      header: null,
    };
  
    constructor(props) {
      super(props)
      this.state = {
        email: null,
        password: null,
        loading: false,
      }
    }
  
    
    render() {
      return (
        this.state.loading? <ActivityIndicator/> : 
        <View style={styles.container}>
          <View style={styles.space} />
  
          <View style={styles.imageContainer}>
            <Image source={imageLogo} style={styles.logo} />
          </View>
  
          <KeyboardAvoidingView style={styles.form} behavior="padding" enabled>
  
            <Text>Email</Text>
            <View style={styles.inputContainer}>
              <TextInput
                editable={true}
                selectionColor="#428AF8"
                underlineColorAndroid="#428AF8"
                style={styles.textInput}
                placeholder="Enter your email"
                autoCorrect={false}
                keyboardType="email-address"
                returnKeyType="next"
                onChangeText={(email) => this.setState({ email })}
                value={this.state.email}
                ref="email"
              />
            </View>
  
            <Text>Password</Text>
            <View style={styles.inputContainer}>
              <TextInput
                editable={true}
                selectionColor="#428AF8"
                underlineColorAndroid="#428AF8"
                style={styles.textInput}
                placeholder="Enter your password"
                secureTextEntry={true}
                returnKeyType="done"
                onChangeText={(password) => this.setState({ password })}
                value={this.state.password}
                ref="password"
              />
            </View>
  
            <TouchableOpacity style={styles.buttonStyle} onPress={this._signInAsync}>
              <Text style={styles.buttonTextStyle}>Login</Text>
            </TouchableOpacity>
  
          </KeyboardAvoidingView>
  
          <View style={styles.space} />
        </View>
      );
    }
  
    _signInAsync = async () => {
      console.log(`Sign in Async`);
      if(this.state.email != null && this.state.password != null){
  
        fetch(domain + "telemed/login"/*"https://apps.ixanos.co.tt/telemed/login" "http://192.168.254.46:3000/telemed/login"*/, {
          method: "POST",
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: this.state.email,
            password: this.state.password
          })
        }).then(response => response.json())
          .then(resp => {
            console.log('Login response:',resp);
            if (resp.success) {
              console.log('Login success');
              this.setState({loading: true});
    
              try {
                AsyncStorage.setItem('userToken', resp.token)
                this.props.navigation.navigate('Main');
              } catch(e) {
                console.log('Set userToken Error:',e);
                alert('Login error 1: ' + e);
              }
    
            } else {
              console.log('Login failure.')
    
              Alert.alert('Login Error', 'Invalid login credentials.',
                [
                  { text: 'OK', onPress: () => console.log('OK Pressed') },
                ]
              )
    
            }
          })
          .catch(err => {
            console.log('Login catch error:', err);
            alert('Login error 2: ' + err)
          })
      }
      else{
        Alert.alert('Login Error', 'Email and password cannot be empty.',
                [
                  { text: 'OK', onPress: () => console.log('OK Pressed') },
                ]
              )
      }
  
    };
  }

  
const styles = StyleSheet.create({
    space: {
      flex: 1
    },
    container: {
      flex: 1,
      backgroundColor: "#FFF",
      alignItems: "center",
      justifyContent: "space-between"
    },
    inputContainer: {
      marginBottom: 10
    },
    buttonStyle: {
      width: "100%",
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#428AF8",
      marginBottom: 12,
      marginTop: 30,
      paddingVertical: 12,
      borderRadius: 4,
      borderWidth: StyleSheet.hairlineWidth,
      borderColor: "rgba(255,255,255,0.7)"
    },
    buttonTextStyle: {
      color: "#FFF",
      textAlign: "center",
      height: 20
    },
    logo: {
      flex: 1,
      width: 200,
      height: 120,
      resizeMode: 'contain',
    },
    imageContainer: {
      flex: 1,
      marginTop: 20,
    },
    form: {
      flex: 4,
      justifyContent: "center",
      textAlign: 'center',
      width: "80%",
    },
    textInput: {
      height: 40,
      ...Platform.select({
        ios: {
          borderColor: "#BEBEBE",
          borderBottomWidth: StyleSheet.hairlineWidth
        },
        // The underline on Android is slightly misaligned so
        // we fix it by adding a left padding here...
        android: {
          paddingLeft: 6
        }
      })
    },
  })