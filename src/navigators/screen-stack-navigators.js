import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";
import CallScreen from "../screens/CallScreen";
import SettingsScreen from "../screens/SettingsScreen";
import ProfileScreen from "../screens/ProfileScreen";
import NotesScreen from "../screens/NotesScreen";
import MapScreen from "../screens/MapScreen";
import HelpScreen from "../screens/HelpScreen";

//Add navigators with screens in this file
export const CallNavigator = createStackNavigator({
  Call: { screen: CallScreen }
});

export const SettingsNavigator = createStackNavigator({
  Settings: { screen: SettingsScreen }
});

export const ProfileNavigator = createStackNavigator({
  Profile: { screen: ProfileScreen }
});

export const NotesNavigator = createStackNavigator({
  Notes: { screen: NotesScreen }
});

export const MapNavigator = createStackNavigator({
  Map: { screen: MapScreen }
});

export const HelpNavigator = createStackNavigator({
  Help: { screen: HelpScreen}
});
