import React from "react";
import Ionicons from "react-native-vector-icons/Ionicons";
import { createBottomTabNavigator } from "react-navigation";
import {
  CallNavigator,
  ProfileNavigator,
  NotesNavigator,
  MapNavigator,
  HelpNavigator
} from "./screen-stack-navigators";

const getTabBarIcon = (navigation, focused, tintColor) => {
  const { routeName } = navigation.state;
  let IconComponent = Ionicons;
  let iconName;
  if (routeName === "Call") {
    iconName = "ios-videocam";
  } else if (routeName === "Profile") {
    iconName = "ios-contact";
  } else if (routeName === "Notes") {
    iconName = "ios-clipboard";
  } else if (routeName === "Map") {
    iconName = "ios-map";
  } else if (routeName === "Help") {
    iconName = "ios-help";
  }

  return <IconComponent name={iconName} size={25} color={tintColor} />;
};

const BottomTabNavigator = createBottomTabNavigator(
  {
    Call: CallNavigator,
    Profile: ProfileNavigator,
    Notes: NotesNavigator,
    Map: MapNavigator,
    Help: HelpNavigator
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) =>
        getTabBarIcon(navigation, focused, tintColor)
    }),
    tabBarOptions: {
      activeTintColor: "black",
      inactiveTintColor: "gray"
    }
  }
);

export default BottomTabNavigator;
