import React from "react";
import { createDrawerNavigator, createAppContainer, createSwitchNavigator } from "react-navigation";
import BottomTabNavigator from "./bottom-tab-navigator";
import { SettingsNavigator } from "./screen-stack-navigators";
import SignInScreen from '../screens/SignInScreen';
import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {
  ActivityIndicator,
  View,
} from 'react-native';

const DrawerNavigator = createDrawerNavigator({
  /*To have a header on the drawer screens, 
        there must be a navigator that includes the screen you want to add to the drawer navigator.
        See 'screen-stack-navigator' file*/
  Home: BottomTabNavigator,
  Settings: SettingsNavigator
});


class AuthLoadingScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };


  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Get the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {

    try {
      const userToken = await AsyncStorage.getItem('userToken')
      console.log('User token:',userToken);
      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      this.props.navigation.navigate(userToken ? 'Main' : 'Auth');
    } catch(e) {
      console.log('Get userToken Error:',e);
      alert('Login error 3: ' + e);
    }

  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <ActivityIndicator />
      </View>
    );
  }
}

const Drawer = createAppContainer(createSwitchNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  AuthLoading: AuthLoadingScreen,
  Main: DrawerNavigator,
  Auth: SignInScreen,
},
  {
    initialRouteName: 'AuthLoading',
  }
));

export default Drawer;
